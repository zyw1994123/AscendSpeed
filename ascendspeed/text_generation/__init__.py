# Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.

from .generation import beam_search
from .utils import greedy_search_or_sampling
